.PHONY: all clean update

TARGET ?= main

all: $(TARGET).pdf


$(TARGET).pdf: $(TARGET).md
	pandoc -f markdown -t beamer --slide-level 2 $(TARGET).md -o $(TARGET).pdf


clean:
	rm *.pdf


update:
	git add .
	git commit -m "update: `date +%b%d | tr -s '[:upper:]' '[:lower:]'` meeting"
	git tag slide-`date +%y%m%d`
	git push