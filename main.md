---
marp: true
title: Progress Update
author: Weijie Huang
institute: Fierce Lab
date: \today
theme: Coffer
math: true
header-includes: |
            \usepackage{xcolor}
---

## Context: Two possible papers

- LAVA VM
  - **Objective**: Complete mediation for all *operations*
    - Data flow: ld/st
    - Control flow: jmp/call/ret
    - Object lifecycle: stack/heap/global variable management
  - **Property**: Mapping each `op` to `lava_op` with CFI enforced
  - **Implementation**: LLVM hooking
    - Indirect jmp/call handling: call gate

- SoftCaps
  - **Objective**: Performance evaluator? SFI optimization for LAVA?
  - **Implementation**: Library runtime
    - VA as identifier
    - Data structure: object/subject permissions
    - Local access optimization: range-based fast check (subheap)

---

## Current progress

- Completed SoftCaps implementation
  - Indirect jmp: done in codegen part
  - Stack allocation & perms: naive version, caller stack accessible to callee
- Refined structure for publishing & benchmarking (w/ Yudi)
- **WIP**: design doc & figure

---

## Notes

evaluate the runtime: 


obj-enc: repr for explainability & method for automatic compartmentalization, notion of sepatability
-> How to extract a meaningful priv repr from src / model compartmentalization over it / automatic labeling / eval from priv and threat resprective

=> end-to-end compiler+runtime (LAVA)
- frontend
- backend (initial softcaps): translate+load+enforce+optimize

langsec: threat thru interface? 
ndss: indirect analysis

backend: profile-guided optimization (initial goal)

challenges:
1. map ir-level to binary, load&enforce, portable
  - translate names down to binary level
  - va exists, symbol table
2. optimize runtime (software-based mechanism)
  - data structures
  - lookup
3. optimize compartmentalization
  - region-based assumption
  - taking over allocators
4. proving hooks are correct / obj access limited (formal verification?)
  - prove policy correctness (ir level -> binary level)
    - non-bypassable proof (doable, for LAVA op transformation)
  - prove functionality correctness (backend)
  - prove optimizations/transformations

---

LAVA abstraction

SoftCaps: a backend w/ non-bypassbale hooking property

translation & enforcement on **ePCG**

- generic monitor framework to implement abstraction or sandbox/compartmentalization
- problem: 
  - platform for profile-guided optim?
  - consistency btw policy and impl?
  - portable, generic runtime to enforce&eval, (and is pretty fast?)

possible arguments:

1. auto-compartmentalization: practical & scalable & least-priv
  - performance implication
2. portably mapping & guarantee enforcing of compartmentalization / least-authority policies
  - slow runtime, worth it?
  - compare SoftCaps, MPK, CHERI
3. hooking & sandbox runtime (eBPF), helps w/ compartmentalization
4. ...

paper focus: mechanism

TODO: write design sec (bottom up), starting from mapping/lowing problem

- * non-bypassable, portable, certified lowering
- inlined sandbox "runtime"/monitor
- binding to addr
- other optim...

---

(yudi)
LAVA interface: interactive tool, universal representation of program structure -> lowering, do stuff in runtime
